<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProjectsController extends Controller
{
    //

    // public function index($username)
    public function index()
    {
        $username = env('USER_NAME');
        return redirect('projects/' . $username);
    }
    public function getUserProject($username)
    {
        $token = env('GITLAB_TOKEN');
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/" . $username . "/projects";
        $header = array('headers' => array('access_token' =>  $token));
        $response = $client->get($uri, $header);
        $projectLists = json_decode($response->getBody()->getContents());
        return view('projects.index', compact('projectLists'));
    }

    public function create()
    {
        // dd('test');
        return view("projects.create");
        // return view("artikel.admin.create");
    }
    public function postUserProject(Request $request)
    {
        // dd($request->projectName);
        $namespace_id = env('NAMESPACE_ID');
        $token = env('GITLAB_TOKEN');
        $client = new Client();

        $uri = "https://gitlab.com/api/v4/projects";
        // $header = array('headers' => array('PRIVATE-TOKEN' =>  $token));
        // $body=
        $params = [
            'headers' => ['PRIVATE-TOKEN' =>  $token,],
            'form_params' => [
                'name' =>  $request->projectName,
                'description' =>  $request->description,
                'namespace_id' => $namespace_id,
                'visibility' => 'public',
            ]
        ];
        $response = $client->post($uri, $params);
        return redirect('/projects');
    }

    public function starProject(Request $request)
    {
        // dd($request->projectName);
        $namespace_id = env('NAMESPACE_ID');
        $token = env('GITLAB_TOKEN');
        $client = new Client();

        $uri = "https://gitlab.com/api/v4/projects/:id/star";
        // $header = array('headers' => array('PRIVATE-TOKEN' =>  $token));
        // $body=
        $params = [
            'headers' => ['PRIVATE-TOKEN' =>  $token,],
            'form_params' => [
                'name' =>  $request->projectName,
                'description' =>  $request->description,
                'namespace_id' => $namespace_id,
                'visibility' => 'public',
            ]
        ];
        $response = $client->post($uri, $params);
        return redirect('/projects');
    }
}
