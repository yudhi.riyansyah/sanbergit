@extends('layouts.master_admin')
@section('content')
    <form action="{{url('projects/create')}}" method="post">
       <input type="text" required="required" class="form-control"name="projectName" placeholder="Project Name" id="projectName">
       <br>
       <textarea name="description" required="required" class="form-control" id="description" cols="30" rows="10">
       </textarea>
       <br>
        {{ csrf_field() }}
       <button  id="submit" class="btn btn-primary"  type="submit"> Create New Project</button>
       <br>
   </form>
@endsection