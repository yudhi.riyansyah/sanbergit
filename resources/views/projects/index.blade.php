@extends('layouts.master_admin')
@section('content')
    {{-- <h1>{{$plStandings->competition->name}}</h1> --}}
{{-- <p>Last Update : {{$plStandings->competition->lastUpdated}} </p> --}}
{{-- <td> <a href="#" id="toggle_star" class="toggle_star btn btn-primary addCart"> Cart</a></td> --}}
    <table class="table table-striped">
        <tr>
            <th>Project Id</th>
            <th>Name</th>
            <th>description</th>
            <th>web_url</th>
            <th>star_count</th>
            <th>owner</th>
        </tr>
        @foreach ( $projectLists as $projectList)
            <tr>
                <td> {{$projectList->id}}</td>
                <td> {{$projectList->name}}</td>
                <td> {{$projectList->description}}</td>
                <td> <a href="{{$projectList->web_url}}">{{$projectList->web_url}}</a></td>
                <td> <button class ="btn btn-min "type="submit">-</button> <i class="toggle_star far fa-star"></i><button class ="btn btn-plus" type="submit">+</button> {{$projectList->star_count}} </td>
                <td> {{$projectList->namespace->name}}</td>

            </tr>
        @endforeach
    </table>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {

            $('.toggle_star').each(function(i, obj) {
                //test
            });

            $(".toggle_star").click(function () {
                //  alert($(this)->$projectList->id);
                if ($(this).hasClass("far")){
                    // alert("click");
                    $(this).removeClass( "far" );
                    $(this).addClass( "fas" );
                }else {
                    // alert("clock");
                    $(this).removeClass( "fas" );
                    $(this).addClass( "far" );
                }
            });

            $(".toggle_star").mouseover(function () {
                });

        });
    </script>
@endsection

{{-- {
        "id": 12079218,
        "description": "",
        "name": "sanbergit",
        "name_with_namespace": "yudhi riyansyah / sanbergit",
        "path": "sanbergit",
        "path_with_namespace": "yudhi.riyansyah/sanbergit",
        "created_at": "2019-04-29T07:10:07.935Z",
        "default_branch": "master",
        "tag_list": [],
        "ssh_url_to_repo": "git@gitlab.com:yudhi.riyansyah/sanbergit.git",
        "http_url_to_repo": "https://gitlab.com/yudhi.riyansyah/sanbergit.git",
        "web_url": "https://gitlab.com/yudhi.riyansyah/sanbergit",
        "readme_url": "https://gitlab.com/yudhi.riyansyah/sanbergit/blob/master/README.md",
        "avatar_url": null,
        "star_count": 0,
        "forks_count": 0,
        "last_activity_at": "2019-04-29T07:10:07.935Z",
        "namespace": {
            "id": 3843059,
            "name": "yudhi.riyansyah",
            "path": "yudhi.riyansyah",
            "kind": "user",
            "full_path": "yudhi.riyansyah",
            "parent_id": null
        }
    } --}}